+===================================+
| WooCommerce JNE Shipping ( Free Version ) |
+===================================+
Developer: ItsMeFurZy
Author URL : http://itsmefurzy.blogspot.com/
Plugin URL : http://www.agenwebsite.com/

Plugin untuk WooCommerce dengan penambahan metode shipping JNE, dilengkapi dengan mata uang Indonesia (Rp).

+===================================+
| Installation |
+===================================+
1. Unzip files.
2. Upload "woocommerce-jne" folder into your plugins directory.
3. Activate the plugin.
4. Go to WooCommerce > Settings > Shipping > JNE Shipping.
5. Configure settings to your needs.
6. Have fun!

+===================================+
| Change Log |
+===================================+
Version 4.0.1
- Perbaikan perhitungan ongkos kirim JNE
- Update tarif JNE kota Jakarta

Version 4.0.0
- Perbaikan beberapa bug dan error
- Update penyederhanaan WooCommerce JNE Shipping
- Penambahan fitur default weight
- Perubahan format data menjadi KOTA,HARGA_OKE,HARGA_REG,HARGA_YES
- Penambahan fitur otomatis ongkos kirim ( Limited Version )
- Penambahan fitur JNE Tracking ( Limited Version )
- Pengembalian fitur Kode Pos 

Version 3.0.1
- Update harga JNE Jakarta 2013

Version 3.0.0
- Perbaikan fitur asuransi
- Perubahan style list data
- Penambahan edit data
- Penghapusan Textarea data
- Penghapusan negara yang diizinkan
Version 2.0.0
- Penambahan support WooCommerce versi 2.0.1
- Perbaikan beberapa bug dan error
- Penambahan fitur asuransi
- Penambahan fitur check update
- Penambahan berapa banyak kota

Version 1.0.2
- Penambahan fitur Import City by Text
- Mudah untuk mengedit kembali dari Text Area yang sudah ada
- Perubahan link credits

Version 1.0.1
- Updated Opsi JNE REG/OKE/JNE *Premium Version
- Pilihan Negara yang diizinkan
- Penambahan fungsi jika kota kosong maka opsi jasa pengiriman tidak ada dan tidak bisa mengirim barang
- Pembetulan fungsi - fungsi yang error
- Penambahan Link Video Tutorial

Version 1.0.0
- Product Launching
